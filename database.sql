-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 05, 2020 at 08:18 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exam_tehn_apli_web_an2_sem1_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `examen_database`
--

DROP TABLE IF EXISTS `examen_database`;
CREATE TABLE IF NOT EXISTS `examen_database` (
  `id` int(250) NOT NULL AUTO_INCREMENT,
  `nume` varchar(250) NOT NULL,
  `categorie` varchar(250) NOT NULL,
  `imagine` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `examen_database`
--

INSERT INTO `examen_database` (`id`, `nume`, `categorie`, `imagine`) VALUES
(1, 'Port 1', 'Appetizers & Soups', 'port1.jpg'),
(2, 'Port 2', 'Appetizers & Soups', 'port2.jpg'),
(3, 'Port 3', 'Appetizers & Soups', 'port3.jpg'),
(4, 'Port 4', 'Sauces & Dides', 'port4.jpg'),
(5, 'Port 5', 'Sauces & Dides', 'port5.jpg'),
(6, 'Port 6', 'Sauces & Dides', 'port6.jpg'),
(7, 'Port 7', 'Desserts', 'port7.jpg'),
(8, 'Port 8', 'Desserts', 'port8.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
