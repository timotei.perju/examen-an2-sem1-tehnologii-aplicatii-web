<!DOCTYPE html>
<html lang="en">

<head>
  <title>AT Restaurant</title>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
</head>

<body>
  <!-- NAVBAR -->
  <div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
      <div class="container">
        <div class="logo"></div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
          aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item red dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Pages
              </a>
              <div class="dropdown-menu dropdown" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">About Us</a>
                <a class="dropdown-item" href="#">Menus</a>
                <a class="dropdown-item" href="#">News & Events</a>
                <a class="dropdown-item" href="#">Chefs</a>
              </div>
            </li>
            <li class="nav-item red dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Portofolio
              </a>
              <div class="dropdown-menu dropdown" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Portofolio 2 Columns</a>
                <a class="dropdown-item" href="#">Portofolio 3 Columns</a>
                <a class="dropdown-item" href="#">Portofolio 4 Columns</a>
              </div>
            </li>
            <li class="nav-item red dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Blog
              </a>
              <div class="dropdown-menu dropdown" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Blog with right sidebar</a>
                <a class="dropdown-item" href="#">Blog with left sidebar</a>
                <a class="dropdown-item" href="#">Article Category Blog</a>
              </div>
            </li>
            <li class="nav-item red dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Post Formats
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Standard Post Format</a>
                <a class="dropdown-item" href="#">Video Post Format</a>
                <a class="dropdown-item" href="#">Audio Post Format</a>
                <a class="dropdown-item" href="#">Gallery Post Format</a>
                <a class="dropdown-item" href="#">Link Post Format</a>
                <a class="dropdown-item" href="#">Quote Post Format</a>
                <a class="dropdown-item" href="#">Status Post Format</a>
                <a class="dropdown-item" href="#">Image Post Format</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- HEADER -->
    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="src/img/slider1.jpg" alt="First slide">
            <h5 class="text-center">THE BEST PLACE</h5>
            <p class="text-center">TO HAVE A GOOD REST!</p>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="src/img/slider3.jpg" alt="Second slide">
            <h5 class="text-center">EXQUISITE CUISINE</h5>
            <p class="text-center">FOR ANY AT RESTAURANT</p>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="src/img/slider1.jpg" alt="Third slide">
            <h5 class="text-center">DISCOVER THE WORL</h5>
            <p class="text-center">OF GREAT TASTES</p>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <!-- ADD WELCOME -->
    <div class="container-fluid text-center">
      <div class="mx-2 my-2">
        <strong>
          <h2>WELCOME TO <span class="at-restaurant">AT RESTAURANT</span></h2>
        </strong>
      </div>
      <div id="bacon" class="min-font">
      </div>
      <hr>
      <div class="row slideanim">
        <div class="col-sm-3 ">
          <i class="fa fa-cutlery fa-4x round" aria-hidden="true"></i>
          <h4 class="mt-2">Best Cuisine</h4>
          <div id="bacon" class="min-font">Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec
            augue semper, in dignissim.
          </div>
        </div>
        <div class="col-sm-3">
          <i class="fa fa-briefcase fa-4x round" aria-hidden="true"></i>
          <h4 class="mt-2">Special Offers</h4>
          <div id="bacon" class="min-font">Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec
            augue semper, in dignissim.
          </div>
        </div>
        <div class="col-sm-3">
          <i class="fa fa-bed fa-4x round"></i>
          <h4 class="mt-2">Good Rest</h4>
          <div id="bacon" class="min-font">Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec
            augue semper, in dignissim.
          </div>
        </div>
        <div class="col-sm-3">
          <i class="fa fa-coffee round fa-4x"></i>
          <h4 class="mt-2">Timings</h4>
          <p id="bacon" class="services-item">Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam
            nec augue semper, in dignissim.
          </p>
        </div>
      </div>
    </div>

    <!-- RESPONSIVE DESIGN SECTION -->
    <div class="container-fluid">
      <div class="row grey space">
        <div class="col-6">
          <img class="d-block w-100" src="src/img/home.png">
        </div>
        <div class=" col-4">
          <h4 class="at-design">Responsive design</h4>
          <p>Lorem ipsum dolor sit amet, consectetur sed do eiusmod tempor incididunt ut labore et dolore.</p>
          <p>Lorem ipsum dolor sit amet, consectetur sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad.
          </p>
          <p>Lorem ipsum dolor sit amet, consectetur sed do eiusmod tempor incididunt.</p>
        </div>
      </div>
    </div>

    <!-- LATEST WORK -->

    <div class="container-fluid color-img">
      <div class="row space color-img">
        <div class="col-12 text-center">
          <h2>LATEST WORK</h2>
          <div id="bacon" class=" text-center">Lorem ipsum dolor sit amet, consectetur sed do eiusmod tempor incididunt
          </div>
        </div>
      </div>
      <div class="row space">
        <div class="col-10 center-block img-thumbnail text-center color-img">
          <img class="img-work" src="src/img/port1.jpg">
          <img class="img-work" src="src/img/port2.jpg">
          <img class="img-work" src="src/img/port3.jpg">
          <img class="img-work" src="src/img/port4.jpg">
          <img class="img-work" src="src/img/port5.jpg">
          <img class="img-work" src="src/img/port6.jpg">
          <img class="img-work" src="src/img/port7.jpg">
          <img class="img-work" src="src/img/port8.jpg">
        </div>
      </div>
    </div>
  </div>

  <hr>



  <!-- UPDATED LATEST WORK WITH PHP -->
  <section id="tabs" class="project-tab">
    <div class="col-12 text-center">
      <h2>UPDATED LATEST WORK WITH PHP</h2>
      <div id="bacon" class=" text-center">Lorem ipsum dolor sit amet, consectetur sed do eiusmod tempor incididunt
      </div>
    </div>
    <hr>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <nav>
            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                aria-controls="nav-home" aria-selected="true">Appetizers & Soups</a>
              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                aria-controls="nav-profile" aria-selected="false">Sauces & Dides</a>
              <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab"
                aria-controls="nav-contact" aria-selected="false">Desserts</a>
            </div>
          </nav>

          <!-- TAB 1 -->

          <!-- Connect with database -->
          <?php
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "exam_tehn_apli_web_an2_sem1_db";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
          
            $sql = "SELECT * FROM examen_database where categorie = 'Appetizers & Soups'";
            $result = $conn->query($sql);
          ?>

          <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
              <table class="table" cellspacing="0">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Image</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php
                      if ($result->num_rows > 0) {
                      // output data of each row
                      while($row = $result->fetch_assoc()) {
                    ?>
                    <td><?php echo $row["nume"]?></td>
                    <td><?php echo $row["categorie"]?></td>
                    <td><?php echo $row["imagine"]?></td>
                  </tr>
                </tbody>

                <?php
                  }
                  } else {
                  echo "0 results";
                  }
                  $conn->close();
                ?>
              </table>
            </div>

            <!-- TAB 2 -->

            <!-- Connect with database -->
            <?php
              $servername = "localhost";
              $username = "root";
              $password = "";
              $dbname = "exam_tehn_apli_web_an2_sem1_db";

              // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
              // Check connection
              if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
              }
          
              $sql = "SELECT * FROM examen_database where categorie = 'Sauces & Dides'";
              $result = $conn->query($sql);
            ?>

            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <table class="table" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Category</th>
                      <th>Image</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <?php
                        if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                      ?>
                      <td><?php echo $row["nume"]?></td>
                      <td><?php echo $row["categorie"]?></td>
                      <td><?php echo $row["imagine"]?></td>
                    </tr>
                  </tbody>

                  <?php
                    }
                    } else {
                    echo "0 results";
                    }
                    $conn->close();
                  ?>
                </table>
              </div>

              <!-- TAB 3 -->

              <?php
                $servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "exam_tehn_apli_web_an2_sem1_db";

                // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "SELECT * FROM examen_database where categorie = 'Desserts'";
                $result = $conn->query($sql);
              ?>

              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                  <table class="table" cellspacing="0">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Image</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <?php
                          if ($result->num_rows > 0) {
                          // output data of each row
                          while($row = $result->fetch_assoc()) {
                        ?>
                        <td><?php echo $row["nume"]?></td>
                        <td><?php echo $row["categorie"]?></td>
                        <td><?php echo $row["imagine"]?></td>
                      </tr>
                    </tbody>

                    <?php
                      }
                      } else {
                      echo "0 results";
                      }
                      $conn->close();
                    ?>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
  </section>

  <!-- IMPORT WEBPACK -->
  <script src="build/script.js"></script>
</body>

</html>